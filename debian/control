Source: golang-github-hashicorp-terraform-json
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any,
               golang-github-davecgh-go-spew-dev,
               golang-github-zclconf-go-cty-dev
Standards-Version: 4.5.0
Homepage: https://github.com/hashicorp/terraform-json
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-hashicorp-terraform-json
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-hashicorp-terraform-json.git
XS-Go-Import-Path: github.com/hashicorp/terraform-json
Testsuite: autopkgtest-pkg-go

Package: golang-github-hashicorp-terraform-json-dev
Architecture: all
Depends: ${misc:Depends},
         golang-github-davecgh-go-spew-dev,
         golang-github-zclconf-go-cty-dev
Description: helper types for the Terraform external data representation
 This package contains data types designed to help parse the data produced
 by two Terraform (https://www.terraform.io/) commands:
  - terraform show -json
  - terraform providers schema -json
 While containing mostly data types, there are also a few helpers to
 assist with working with the data.
 .
 This pcakage also serves as de facto documentation for the
 formats produced by these commands.
